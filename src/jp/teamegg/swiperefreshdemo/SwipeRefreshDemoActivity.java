package jp.teamegg.swiperefreshdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * SwipeRefreshLayoutのデモ画面です。
 * 
 * @author yoshihide-sogawa
 * 
 */
public class SwipeRefreshDemoActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener {

    /** {@link SwipeRefreshLayout} */
    private SwipeRefreshLayout mSwipeRefreshLayout;
    /** {@link ListView} */
    private ListView mListView;

    /** {@link Handler} */
    private Handler mHandler = new Handler();
    /** 更新のための{@link Runnable}オブジェクト */
    private final Runnable mRefreshDone = new Runnable() {
        @Override
        public void run() {
            // 引っ張り出し有効
            mSwipeRefreshLayout.setEnabled(true);
            // 更新アニメーションを非表示
            mSwipeRefreshLayout.setRefreshing(false);
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);

        // 更新中のアニメーション色設定
        mSwipeRefreshLayout.setColorScheme(R.color.color1, R.color.color2, R.color.color3, R.color.color4);

        mListView = (ListView) findViewById(R.id.content);
        mListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, getResources().getStringArray(R.array.kreva_songs)));
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.swipe_refresh_menu, menu);
        return true;
    }

    /**
     * メニューから更新が押された際の処理を行います。
     */
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();

        // 更新処理
        if (id == R.id.force_refresh) {
            mSwipeRefreshLayout.setRefreshing(true);
            refresh();
            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRefresh() {
        refresh();
    }

    /**
     * ダミーの更新処理を行います。
     */
    public void refresh() {
        mHandler.removeCallbacks(mRefreshDone);
        mHandler.postDelayed(mRefreshDone, 2000);
        // 引っ張り出し無効
        mSwipeRefreshLayout.setEnabled(false);
    }

}
